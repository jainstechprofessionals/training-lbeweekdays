
public class MethodExample {

	public boolean evenOrOdd(int a) {
		if(a%2==0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void printPaperSet(boolean b) {
		if(b==true) {
			System.out.println("Student will get set A");
		}else {
			System.out.println("Student will get set B");
		}
	}
	
	public void show() {
		System.out.println("in show");
	}
	

	public static void main(String[] args) {
		MethodExample obj = new MethodExample();		
		boolean c =obj.evenOrOdd(20);	
//		System.out.println(c);
		
		obj.printPaperSet(c);

		obj.show();
	}

}
