package AccessModifier;

public class NonSubCLass {

	public void show() {
		ClassA obj = new ClassA();
		
		obj.defaultMethod();
		obj.protectedMethod();
		obj.publicMethod();
		
	}
	
	

}
