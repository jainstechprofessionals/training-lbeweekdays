
public class StringInJava {

	public static void main(String[] args) {
	

		
		// non primitive 
		// String 
		
		// 1. Literal 
		
		
		
		String str = "rounak";  // Literal 
		String str1 = "rounak"; 
		
		if(str == str1) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
		
		
		// 2. Using new keyword
		
		String str2 = new String("rounak");
		String str3 = new String("Rounak");
		
//		if(str2 == str3) {
//			System.out.println("equal");
//		}else {
//			System.out.println("not equal");
//		}
		
		
		if(str2.equals(str3)) {
			System.out.println("Equal");
		}else {
			System.out.println("not equal");
		}
//		if(str2.equalsIgnoreCase(str3)) {
//			System.out.println("Equal");
//		}else {
//			System.out.println("not equal");
//		}
		
		

	}

}
