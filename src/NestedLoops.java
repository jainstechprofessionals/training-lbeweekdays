
public class NestedLoops {

	public static void main(String[] args) {
		
		
		NestedLoops pqr= new NestedLoops();
		
		

		for (int i = 0; i < 5; i++) {

			for (int j = i; j >= 0; j--) {
				System.out.print("*");
			}

			System.out.println();
		}

		for (int i = 0; i < 5; i++) {
			
			for (int j = i; j < 5; j++) {
				System.out.print("*");
			}
				
			System.out.println();
		}

	}

}
