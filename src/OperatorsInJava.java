
public class OperatorsInJava {

	public static void main(String[] args) {

		int a = 41;
		int b = 20;

		// Arithmetic Operator

//		System.out.println(a+b);
//		System.out.println(a-b);
//		System.out.println(a*b);
//		System.out.println(a/b); //2 
//		System.out.println(a%b); //1 

		// Assignment Operator
		// =,+=,-=,/=,%=
		a = 43;
//		a=a+2;
//		a+=2;

		// Relational Operator
		// <,> ,>=,<=, ==,!=

//		boolean g;
//		g= (a==43);
//		System.out.println(g);
//		System.out.println(a>43);
//		System.out.println(a<43);
//		System.out.println(a!=44);

		// Logical Operator
		
		// && , ||
		
//		System.out.println(a==43 && a>40);
		
//		unary 
//		 exp++,exp --, ++exp, --exp
		int temp ;
		int e = 10;
//		System.out.println(e);
//		e++;
//		System.out.println(e);
		
//		temp = e++;
//		temp = ++e;  // temp = 11
		
//		temp = e++ + e++; // temp = 10 + 11 
//		temp = e++ + ++e; // temp = 10 + 12
		temp = e-- - --e;  // temp = 10 - 8 
		
		System.out.println(temp); //
		System.out.println(e); // 
		
		
	}

}
