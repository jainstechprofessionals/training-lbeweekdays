
public class BackUpVariable {

	public static void main(String[] args) {
		
		BackUpVariable test = new BackUpVariable();
		
		
		int a=10;	
		a=2;
		a=3434;
		
		
		boolean b = true; // 1 bit
		char c='a';  // 2 byte
		
		byte d = 12;  // 1 byte -128 to 127
		short s= 328;// 2 byte   -32768 to 32767
		int i=89; // 4 byte 
		long l=90; // 8 byte
		
		
		float f = 23.2f; // 4
		double d1 =34.32d; // 8
	
		
//		type conversion
//		1. implict 
//		2. Explicit
		
//		s=d;
//		d=(byte) s;
//		System.out.println(i);
//		f=i;
//		System.out.println(f);
//		i=(int) f;
	}

}
