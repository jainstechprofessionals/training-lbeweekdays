package InterfaceInJava;

 class ClassB implements ParentIn {

	public void show() {
		System.out.println("in show of ClassB");
	}

	public void display() {
		System.out.println("in display of ClassB");

	}

	public void sum(int i, int j) {
		System.out.println("in sum of ClassB");

	}

	@Override
	public void getRate() {
		// TODO Auto-generated method stub
		
	}

}

// for Format : ctrl +shift+f
// Comment the code :  ctrl+? 
//  unComment the code : ctrl+? 
//Comment the code :  ctrl+shift+?
//unComment the code : ctrl+shift+|
