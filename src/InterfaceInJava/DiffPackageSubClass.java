package InterfaceInJava;

import AccessModifier.ClassA;

public class DiffPackageSubClass extends ClassA {

	
	public void show() {
		super.protectedMethod();
		super.publicMethod();
	}
	
}
