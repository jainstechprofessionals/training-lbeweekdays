package InterfaceInJava;

public class ChildClass1 implements ParentIn,Interface2{

	@Override
	public void show() {
		System.out.println("in show ChildClass1");
		
	}

	@Override
	public void display() {
	System.out.println(" in display ChildClass1");
		
	}

	@Override
	public void sum(int i, int j) {
		System.out.println(i+j);
		System.out.println("in sum ChildClass1");
		
	}

	@Override
	public void name() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void getRate() {
		System.out.println("in getRate ChildClass1");
		
	}

}
