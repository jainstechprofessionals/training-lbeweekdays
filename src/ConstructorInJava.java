
public class ConstructorInJava {

	String str;
	
	public ConstructorInJava(){
		System.out.println("in default constructor");
	}
	
	public ConstructorInJava(String a){
		str=a;
		System.out.println("in parame constructor");
	}
	
	
	public void compare(String st) {
		if(str.equals("st")) {
			System.out.println("equl");
		}else {
			System.out.println("not Equal");
		}
	}
	
	public void setter(String s) {
		str=s;
	}
	public String getter() {
		return str;
	}
	
	
	public int sum() {
		System.out.println("in sum");
		return 9;
	}

	public static void main(String[] args) {

		ConstructorInJava obj = new ConstructorInJava("rounak");

		
//		ConstructorInJava obj1 = new ConstructorInJava();
//		obj.compare("rounak");
//		
//		
//		obj1.compare("asf");
//		int i = obj.sum();
//		System.out.println(i);
//		
		obj.setter("ruchi");
		String st = obj.getter();
		System.out.println(st);
		
		// Allure 
		
	}

}
