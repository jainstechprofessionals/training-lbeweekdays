package CollectionsInJava;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapInJava {

	public static void main(String[] args) {
		
		TreeMap<Integer,String> hs = new TreeMap<Integer,String> ();
	
		
		hs.put(102, "shewta");
		hs.put(103, "ruchi");
		hs.put(104, "charu");
		hs.put(101, "rounak");
		hs.put(101, "sadfsafklj");
//		hs.put(null, "rounak");
		
		System.out.println(hs.size());
		System.out.println(hs);
		
		System.out.println(hs.get(101));
		
		
//		 Set s = hs.entrySet();
//		
//		Iterator it =  s.iterator();	
//		while(it.hasNext()) {
//		Map.Entry en=	(Map.Entry) it.next();
//		System.out.print(en.getKey());
//		System.out.print("  : " +en.getValue());
//		System.out.println();
//		}
		
		for(Map.Entry m : hs.entrySet()) {			
			System.out.print(m.getKey());
			System.out.println(m.getValue());
			System.out.println();
		}
		
		
		
	}

}
