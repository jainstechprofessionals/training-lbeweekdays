package CollectionsInJava;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetsInJava {

	public static void main(String adf[]) { 
		
//		Set<String> st = new HashSet<String>();
			
		HashSet<String> st = new HashSet<String>();
		st.add("rounak");
		st.add("vikas");
		st.add("julee");
		st.add("shewta");
		st.add("ruchi");
		st.add(null);
		st.add("ruchi");
		System.out.println(st);
		System.out.println(st.size());
		
		System.out.println(st.contains("ruchi"));
		
		Iterator<String> itr = st.iterator();
		
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
		
		
	
		
		
		
		
	}
	
}
