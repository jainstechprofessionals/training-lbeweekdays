
public class OverLoadingInJava {

//	public int sum(int i, int j) {
//		return i + j;
//	}

	public int sum(int i, int j, int k) {
		return i + j + k;
	}

	public float sum(int i, float f) {
		return i + f;
	}
	
	

	public static void main(String[] args) {

		OverLoadingInJava obj = new OverLoadingInJava();

//		int a = obj.sum(3, 4);
//		System.out.println(a);
		int b = obj.sum(12, 23, 43);
		System.out.println(b);

		float f = obj.sum(12, 34.9f);
		System.out.println(f);
		
		float g =obj.sum(1, 222222222);
		System.out.println(g);
	}

}
