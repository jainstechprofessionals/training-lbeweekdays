
public class StaticVariableInJava {

	int roll;
	String name;
	static String college = "lnct";
 
	final int p =23;
	
	
	public StaticVariableInJava(int r, String n) {
		
		roll = r;
		name = n;

	}

	static {
		
		System.out.println("in static");
	}

	public static void display() {

		System.out.println("in display");
	}

	public void show() {

		System.out.println(roll);
		System.out.println(name);
		System.out.println(college);
	}

	public static void main(String[] args) {

		StaticVariableInJava.display();

		StaticVariableInJava obj = new StaticVariableInJava(1, "rounak");
		StaticVariableInJava obj1 = new StaticVariableInJava(2, "ruchi");

	
		
		obj.show();
		obj.name = "Raunak Jain";
		StaticVariableInJava.college = "oriental";
		obj.show();

		obj1.show();
		
		

	}

}
