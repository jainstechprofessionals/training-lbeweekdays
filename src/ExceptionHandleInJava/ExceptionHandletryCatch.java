package ExceptionHandleInJava;

public class ExceptionHandletryCatch {

	
	
	
	public void ee() {
		int a = 10;
		int b = 2;
		String str = "rounak";
		int arr[] = new int[5];
		
		try {
			System.out.println(a / b);
			System.out.println(str.length());

			arr[5]=0;
			
			System.out.println("hello");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void show() {
		int a = 10;
		int b = 2;
		String str = "rounak";
		int arr[] = new int[5];
		
		try {
			System.out.println(a / b);
			System.out.println(str.length());

//			arr[5]=0;
			
			System.out.println("hello");
		} catch (ArithmeticException e) {
			System.out.println("Please enter a valid value");
			e.printStackTrace();
		} catch (NullPointerException e) {
			System.out.println("string is null");
			e.printStackTrace();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		
		finally {
			System.out.println("terminate browser");
		}

	}

	public static void main(String[] args) {

		ExceptionHandletryCatch ojbj = new ExceptionHandletryCatch();
		ojbj.show();
		// 1. Checked Exception
		// 2. Unchecked Exception
		// 3. Errors

		// try , catch , throw,throws, finally

	}

}
