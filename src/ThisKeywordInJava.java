
public class ThisKeywordInJava {

	int a; //
	int b;
	
	
	ThisKeywordInJava(int a,int b){
		this.a=a;
		this.b=b;
	}
	
	ThisKeywordInJava(){
		this(10);
		a=10;
		System.out.println("in thiskeyword");
	}

	ThisKeywordInJava(int i){
//		this();
		a=i;
		System.out.println("in parameter thiskeyword");
	}
	
	ThisKeywordInJava(int i,String str){
//		this();
		System.out.println("in parameter thiskeyword");
	}
	
	
	
	
	public void print() {
		System.out.println(a);
		int a;
		a=90;
		System.out.println(a);
		System.out.println(this.a);
		
		System.out.println("in print");
	}
	
	public  void display() {
		this.print();
		System.out.println("in display");
	}
	public static void main(String[] args) {
		ThisKeywordInJava obj = new ThisKeywordInJava(12,13);
		obj.print();
	}

}
