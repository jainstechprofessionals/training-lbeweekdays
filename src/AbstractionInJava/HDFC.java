package AbstractionInJava;

public class HDFC extends PolicyBaazar{

	@Override
	public int getRateOfInterest() {
		
		System.out.println("in HDFC");
		return 7;
	}

	@Override
	public int fixedDepositIntrestRate() {
		System.out.println("in HDFC");
		return 4;
	}

}
