
public class Cosn {
	int b;

	Cosn() {
		System.out.println("in default");
	}

	Cosn(int a) {
		b = a;
		System.out.println("in non param");
	}

	public void show() {
		System.out.println(b)	;
	}

	public static void main(String[] args) {

		Cosn obj = new Cosn();

		Cosn obj1 = new Cosn(10);
		obj.show();
		obj1.show();

	}

}
