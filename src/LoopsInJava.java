
public class LoopsInJava {

	public static void main(String[] args) {

		// entry (while loop, For loop)
//		exit   (do while)

//		int a = 2;
//		System.out.println("before loop");
//		while (a < 2) {
//			System.out.println(a);
//			a = a + 2;
//		}
//
//		System.out.println("after loop");

//		********************* 
//		int n =2;
//		
//		for(int i =n ; i <=20; i=i+2) {
//			System.out.println(i);
//		}

//		*********************
		int a =2;
		
		do {
			System.out.println(a);
			a=a+2;
		}while(a<2) ;
//		

		// WAP Prime number (13)

	}

}
