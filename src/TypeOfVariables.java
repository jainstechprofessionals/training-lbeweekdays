
public class TypeOfVariables {

	// local
	// instance (Global)
	// Static

	int a; // instance variable

	public TypeOfVariables() {
		a = 50;
	}

	public void init(int i) {
		a = i;
	}

	public void print() {
		System.out.println(a);
	}

	public void show() {
		a = 40;
		System.out.println(a);
		int a; // local variable
		a = 10;
		System.out.println(a);
		System.out.println("in show");
	}

	public void display() {
		System.out.println("in display");
		System.out.println(a);
	}

	public void sum(int a) {
		System.out.println(a);
	}

	public void temp() {
		System.out.println(a);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TypeOfVariables obj = new TypeOfVariables();
		obj.print();
		obj.show();
		obj.display();
		obj.sum(7888);
		obj.temp();
	}

}
